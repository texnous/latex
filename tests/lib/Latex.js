/**
 * @fileoverview Tests of general LaTeX definitions.
 * This file is a part of TeXnous project.
 *
 * @copyright TeXnous project team (http://texnous.org) 2016-2017
 * @license LGPL-3.0
 *
 * This unit test is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This unit test is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this unit
 * test; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA.
 */

"use strict";

const Latex = require("../../sources/lib/Latex"); // general LaTeX definitions

let state, operation;

/**
 * Latex namespace tests
 * @author Kirill Chuvilin <k.chuvilin@texnous.org>
 */
module.exports = {
	"Lexeme": {
		"keys = values": test => {
			Object.keys(Latex.Lexeme).forEach(key => test.equal(Latex.Lexeme[key], key));
			test.done();
		}
	},
	"Mode": {
		"keys = values": test => {
			Object.keys(Latex.Mode).forEach(key => test.equal(Latex.Mode[key], key));
			test.done();
		}
	},
	"State": {
		"constructor": test => {
			test.throws(() => new Latex.State(""));
			test.throws(() => new Latex.State(null));
			test.doesNotThrow(() => new Latex.State());
			test.doesNotThrow(() => new Latex.State([]));
			test.doesNotThrow(() => new Latex.State({}));
			test.doesNotThrow(() => state = new Latex.State({"MATH": true}));
			test.done();
		},
		"update": test => {
			test.throws(() => state.update(""));
			test.throws(() => state.update(null));
			test.throws(() => state.update());
			test.doesNotThrow(() => state.update([]));
			test.doesNotThrow(() => state.update({}));
			test.doesNotThrow(() => state.update({"TABLE": true}));
			test.done();
		},
		"test": test => {
			test.throws(() => (new Latex.State().test()));
			test.equal((new Latex.State().test({ })), true);
			test.equal((new Latex.State().test({ MATH: false })), true);
			test.equal((new Latex.State().test({ MATH: true })), false);
			test.equal((new Latex.State().test({ MATH: true, TEXT: true })), false);
			test.equal((new Latex.State().test({ MATH: false, TEXT: true })), true);
			test.done();
		},
		"toString": test => {
			test.equal(
				(new Latex.State()).toString(),
				"{ LIST: false, MATH: false, PICTURE: false, TABLE: false, TEXT: true, VERTICAL: false }"
			);
			test.equal(
				(new Latex.State({"MATH": true, "TEXT": false})).toString(),
				"{ LIST: false, MATH: true, PICTURE: false, TABLE: false, TEXT: false, VERTICAL: false }"
			);
			test.equal(
				state.toString(),
				"{ LIST: false, MATH: true, PICTURE: false, TABLE: true, TEXT: true, VERTICAL: false }"
			);
			test.equal(
				state.copy().toString(),
				state.toString()
			);
			test.done();
		}
	},
	"Directive": {
		"keys = values": test => {
			Object.keys(Latex.Directive).forEach(key => test.equal(Latex.Directive[key], key));
			test.done();
		}
	},
	"Operation": {
		"constructor": test => {
			test.throws(() => new Latex.Operation(""));
			test.throws(() => new Latex.Operation(null));
			test.throws(() => new Latex.Operation([]));
			test.throws(() => new Latex.Operation({ }));
			test.throws(() => new Latex.Operation({ directive: Latex.Directive.BEGIN }));
			test.throws(() => new Latex.Operation({ operand: Latex.GROUP }));
			test.doesNotThrow(() => new Latex.Operation());
			test.doesNotThrow(() =>
				operation = new Latex.Operation({ directive: Latex.Directive.BEGIN, operand: Latex.GROUP }));
			test.done();
		},
		"equals": test => {
			test.equal((new Latex.Operation()).equals({ }), false);
			test.equal((new Latex.Operation()).equals((new Latex.Operation())), true);
			test.equal((new Latex.Operation()).equals(operation), false);
			test.equal(operation.equals(new Latex.Operation()), false);
			test.equal(
				(new Latex.Operation({ directive: Latex.Directive.BEGIN, operand: Latex.GROUP })).equals(operation),
				true
			);
			test.equal(
				operation.equals(new Latex.Operation({ directive: Latex.Directive.BEGIN, operand: Latex.GROUP })),
				true
			);
			test.equal(
				operation.equals(new Latex.Operation({ directive: Latex.Directive.BEGIN, operand: Latex.Mode.MATH })),
				false
			);
			test.equal(
				operation.equals(new Latex.Operation({ directive: Latex.Directive.END, operand: Latex.GROUP })),
				false
			);
			test.done();
		}
	}
};
